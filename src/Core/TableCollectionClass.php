<?php
namespace Micro\Core;

class TableCollectionClass extends ModelIteratorClass 
{

    public $list;
    
    public $found = 0;
    public $limit = 0;
    public $page = 0;
    public $offset = 0;
    
    public $tablePrefix;
    public $tableName;
    public $orderTransformTable;
    
    
    
    public $debug = false;
    public $debugQuery = '';
    
    private $_postFilter = array();
    
    public function __construct() {
        parent::__construct();
    }
    
    public function registerPostFilter($action) {
        $this->_postFilter[] = $action;
    }
    
    public function getList($page, $limit = 10, $order, $order_dir, $filters) {       
        $this->page = intval($page);
        $this->limit = ($limit === false)? false : intval($limit);
        if($this->limit !== false && $this->limit == 0) $this->limit = 10;

        $this->offset = $this->page * $this->limit;
        
        $order = $this->makeOrder($order);
        $order_dir = $this->makeOrderDirection($order_dir);
        
        $sql = "SELECT SQL_CALC_FOUND_ROWS {$this->tablePrefix}.*";
        $filters = $this->makeFilter($filters);
        
        $sql .=  (strlen($filters['fields']) > 0)? ', ' . $filters['fields'] : ' ';
        $sql .=  " FROM {$this->tableName} AS {$this->tablePrefix} ";
        $sql .=  $filters['join'];
        $sql .=  $filters['where'];
        
        $sql .= " ORDER BY {$order} {$order_dir} ";

        if($this->limit !== false) {
            $sql .= " LIMIT {$this->offset},{$this->limit}";
        }
        
        $this->db->error = false;
        $this->db->query($sql, $filters['params']);    
        
        if($this->debug) {
        	$this->debugQuery = $this->db->getSql();
        }
        
        if($this->db->error) {
            return array('error'=>true, 'message'=> $this->db->error);
        }
        
        
        
        $list = $this->db->getAll(true);
        $this->found = (int) $this->db->foundRows;
        $this->list =  $this->postProcess($list);
        
        $this->checkPostFilter();
        
        return true;
    }
    
    public function getMetaData() {
    	$meta = array(
    			'found' => $this->found,
    			'offset' => $this->offset,
    			'page' => $this->page,
    			'pages' => ceil($this->found / $this->limit),
    			'limit' => $this->limit
    	);
    	
    	$meta['next'] = ($meta['pages'] > ($meta['page']+1))? $this->page +1 : 0;
    	
    	return $meta;
    }
    
    protected function checkPostFilter() {
        if(empty($this->_postFilter)) return;
        
        foreach($this->_postFilter as $filter) {
            if(is_callable($filter)) {
                foreach($this->list as $k=>$item){
                    $this->list[$k] = call_user_func_array($filter, array($item));
                }
            } elseif(is_array($filter)) {
                $result = array();
                
                foreach($this->list as $k=>$item){
                    $this->list[$k] = array_intersect_key($item, $filter);
                }
            }
        }
    }
    
    protected function postProcess($data) {
        return $data;
    }
    
    protected function makeFilter($filters) {
        return array(
            'join' => '',
            'where' => ' WHERE 1 = 1 ',
            'fields' => '',
            'params' => array()
        );
    }
    
    protected function makeOrder($orderBy) {
        if(!empty($this->orderTransformTable) && isset($this->orderTransformTable[$orderBy])) {
            $orderBy = $this->orderTransformTable[$orderBy];
        }
        
        return $orderBy;
    }
    
    protected function makeOrderDirection($orderDir) {
        return ($orderDir == 'DESC' || $orderDir == 'desc')? 'DESC' : 'ASC';
    }
}