<?php

namespace Micro\Core;

class EmptyObject {
    
    private $store = array();
    
    public function __get($name) {
        if(!isset($this->store[$name])) {
            $this->store[$name] = new self();
        }
        
        return $this->store[$name];
    }
    
    public function __call($name, $args) {
        return false;
    }
    
}