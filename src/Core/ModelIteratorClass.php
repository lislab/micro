<?php 
namespace Micro\Core;

use Micro\Pdo\AppDatabase;

class ModelIteratorClass extends \ArrayIterator
{
    /**
     * @var AppDatabase
     */
    public $db;
    
    private $_transformList;
    
    public function __construct() 
    {
        $this->db = container()->get(AppDatabase::class);
    }
    
    public function toObject() 
    {
        return $this->getObjectVars();
    }
    
    public function toJson() 
    {
        return json_encode($this->getObjectVars(), JSON_UNESCAPED_UNICODE);
    }
    
    private function getObjectVars() 
    {
        $result = array();
        foreach(get_class_vars(get_class($this)) as $property => $value) {
            if(!property_exists(__CLASS__, $property) && property_exists(get_class($this), $property)) {
                if($this->{$property} instanceof \Micro\Core\ModelClass) {
                    $result[$property] = $this->{$property}->getObjectVars();
                } else {
                    $result[$property] = $this->{$property};
                }
            }
        }
        
        return (empty($result))? null : $result;
    }
    
    protected function makeModel($data, $alias = array()) {
        if(empty($data) || !is_array($data)) return;
        foreach($data as $property=>$value) {
            $realName = $property;
            if(!empty($alias) && !empty($alias[$property])) {
                $property = $alias[$property];
                $this->_transformList[$property] = $realName;
            }
            
            if(strpos($property, '_') !== false) {
                $property = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $property))));
            }
            
            if(property_exists(get_class($this), $property)) {
                $this->{$property} = $value;
            }
        }
    }
    
    protected function makeDBobject() {
        $data = $this->getObjectVars();
        if(!empty($this->_transformList)) {
            foreach($this->_transformList as $property=>$realname) {
                if(isset($data[$property])) {
                    $data[$realname] = $data[$property];
                    unset($data[$property]);
                }
            }
        }

        $result = array();
        
        foreach($data as $property=>$value) {
            $property = preg_replace("/([A-Z])/s", "_$1", $property); 
            $property = strtolower($property);
            $result[$property] = $value;
        }
        
        return $result;
    }
    
    protected function databaseUpdate($ignoreFields) {
        $meta = $this->db->getTableMeta($this->getTableName());
        $data = $this->makeDBobject();
        
        if($ignoreFields && is_array($ignoreFields) && !empty($ignoreFields)) {
            foreach($ignoreFields as $field) {
                if(isset($data[$field]) || $data[$field] == NULL) {
                    unset($data[$field]);
                }
            }
        }
        
        $query = array();

        foreach($data as $field => $value) {
            if(!in_array($field, $meta)) continue;
            $query[$field] =  $value;
        }    

        return $this->db->save($query, $this->getTableName(), $this->getPrimaryKey());
    }
        
}


?>