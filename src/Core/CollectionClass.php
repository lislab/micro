<?php
namespace Micro\Core;

class CollectionClass extends \ArrayIterator
{
    
    public function add($value) {
        $this->append($value);
    }
        
    public function find($key, $value, $onlyIndex = false, $onlyField = false, $alwaysReturnObject = false) {  
        $return = new CollectionClass();
    
        if(!$this->count()) {
            if($alwaysReturnObject) return $return;
            return null;
        }
        
        $this->seek(0);
        while($this->valid()) {
            $item = $this->current();
            
            if(is_array($item)) {
                if(!isset($item[$key])) {
                	$this->next();
                	continue;
                }
                if($item[$key] == $value) {
                    if($onlyIndex) {
                       return $key;
                    } elseif($onlyField && isset($item[$onlyField])) {
                        return $item[$onlyField];
                    } else {
                        $return->append($item);
                    }
                }
            } elseif(is_object($item)) {
                if(!property_exists($item, $key)) {
                	$this->next();
                	continue;
                }
                if($item->{$key} == $value) {
                    if($onlyIndex) {
                        return $key;
                    } elseif($onlyField && isset($item[$onlyField])) {
                        return $item[$onlyField];
                    } else {
                        $return->append($item);
                    }
                }
            }
            
            $this->next();
        }
            
        if($alwaysReturnObject) return $return;
        
        return ($return->count())? $return : null;
    }
    
    public function query($key, $value) {
        return $this->find($key, $value, false, false, true);
    }

    public function eq($index) {
        return $this->item($index);
    }
    
    public function item($index) {
        $this->seek($index);
        return $this->current();
    }
    
    public function toObject($oneItem = false) {
        if($oneItem) {
            $result = iterator_to_array($this);
            return array_shift($result);
        } else {
            return iterator_to_array($this);
        }
        
        //$this->storage;
    }
    
}