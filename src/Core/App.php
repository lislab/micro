<?php

namespace Micro;

use Klein\Klein;
use Laminas\HttpHandlerRunner\Emitter\EmitterStack;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Micro\Core\SiteConfigClass;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

class App
{
    /**
     * @var array
     */
    public $config;

    /**
     * @var Klein
     */
    public $router;

    /**
     * @var SiteConfigClass
     */
    public $site;

    /**
     * @var array
     */
    private $components;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var App
     */
    private static $instance;

    public function __construct(ContainerInterface $container)
    {
        self::$instance = $this;

        $this->container = $container;
        $this->config = $container->get('config');
        $this->init();
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function run()
    {
        $this->router->dispatch(null, null, true, $this->router::DISPATCH_CAPTURE_AND_APPEND);
    }

    private function init()
    {
        $this->_fixGlobalVariables();

        if ($this->_useComponent('multisite')) {
            $this->site = new \Micro\Core\SiteConfigClass();

            if (!$this->site->init()) {
                \Micro\App::errorHandler('App error', 'Site not found!');
                return false;
            }
        }

        $this->_router();

        if (isset($this->site->config['maintance']) && $this->site->config['maintance']) {
            $html = file_get_contents($this->site->getLayoutPath() . '/maintance.twig');
            die($html);
        }

        session_start();
    }

    public static function instance()
    {
        return self::$instance;
    }

    /**
     * Include routes list
     *
     * example:
     *
     * --------------------------------------------------------------------------------------------------
     *     // normal route specification
     *   return array(
     *      // Route sting. For more information, see Klein documentation https://github.com/klein/klein.php
     *    'client/item' => array(
     *        'module' => 'Users',        // name of module. required.
     *            'controller' => 'profile',  // name of controller. If not specified, used DefaultController
     *            'action' => 'item',            // name of acton. If not specified, used IndexAction
     *            'query' => 'mixed value',   // Additional parameter passed to the action. No required.
     *            'method' => 'GET'            // Method. If not specified, listen all methods!
     *    )
     *   )
     * --------------------------------------------------------------------------------------------------
     *
     *  // complex route specification (ex. for make API)
     *  return array(
     *    'multiple' => true, // Indicates that the route is complex
     *
     *    // First, specify method type, next specify handler
     *    'GET' => array(
     *            'module' => 'Users',        // name of module. required.
     *            'controller' => 'profile',  // name of controller. If not specified, used DefaultController
     *            'action' => 'item',            // name of acton. If not specified, used IndexAction
     *            'query' => 'mixed value',   // Additional parameter passed to the action. No required.
     *        ),
     *
     *        'POST' => array(
     *            'module' => 'Users',
     *            'controller' => 'remove'
     *        )
     *  )
     *
     *---------------------------------------------------------------------------------------------------
     */
    private function _router()
    {
        $this->router = new \Klein\Klein();
        if ($this->site instanceof \Micro\Core\SiteConfigClass) {
            $prefix = $this->site->prefix;
            $routesFiles = (file_exists(ROOTPATH . $this->site->path . 'routes.php'))
                ? ROOTPATH . $this->site->path . 'routes.php'
                : false;
        } else {
            $prefix = '';
            $routesFiles = (file_exists(ROOTPATH . '/app/routes.php'))
                ? ROOTPATH . '/app/routes.php'
                : false;
        }

        if ($routesFiles) {
            try {
                $siteRoutes = require $routesFiles;
                if (is_array($siteRoutes)) {
                    foreach ($siteRoutes as $route => $param) {
                        if (!empty($param) && !empty($param['method'])) {
                            $this->router->respond($param['method'], $prefix . $route, \Micro\Core\RouteHandler::instance()
                                                                                                               ->appAction($param));
                        } elseif (!empty($param) && !empty($param['multiple']) && $param['multiple']) {
                            foreach ($param as $methodType => $routeHandler) {
                                if (!in_array($methodType, ['GET', 'POST', 'PUT', 'DELETE'])) continue;
                                $this->router->respond($methodType, $prefix . $route, \Micro\Core\RouteHandler::instance()
                                                                                                              ->appAction($routeHandler));
                            }
                        } else {
                            $this->router->respond($prefix . $route, \Micro\Core\RouteHandler::instance()
                                                                                             ->appAction($param));
                        }
                    }
                }
            } catch (Exception $e) {
                $this->errorHandler('App error', 'Error load App routes. Reason: ' . $e->getMessage());
            }
        }

        $this->router->onError(function (...$error) {
            if (!empty($error) && $error[0] instanceof \Klein\Klein) {
                \Micro\App::errorException(...$error);
            } else if (is_string($error[0]) || is_numeric($error[0])) {
                \Micro\App::httpError(...$error);
            } else if ($error[0] instanceof \ErrorException) {
                \Micro\App::httpError($error[0]->getCode(), $error[0]->getMessage(), $error[0]);
            } else {
                \Micro\App::httpError(500, 'Unknown error from unknown handler.');
            }
        });

        $this->router->onHttpError(function (...$error) {
            \Micro\App::httpError($error[4]->getCode(), $error[4]->getMessage(), $error[4]);
        });

        $globalActionCallback = \Micro\Core\RouteHandler::instance()->globalAction();

        $this->router->respond($prefix, $globalActionCallback);
        $this->router->respond($prefix . '[:module]?/[:controller]/?', $globalActionCallback);
        $this->router->respond($prefix . '[:module]?/[:controller]?/[:action]?', $globalActionCallback);
        $this->router->respond($prefix . '[:module]/[:controller]/[:action]/[*:query]', $globalActionCallback);
    }

    private function _useComponent($componentName)
    {
        return (!empty($this->config['components']) && in_array($componentName, $this->config['components']))
            ? true
            : false;
    }

    private function _fixGlobalVariables()
    {
        if (!empty($_SERVER['HTTP_HOST']) && $_SERVER['SERVER_NAME'] != $_SERVER['HTTP_HOST']) {
            $_SERVER['SERVER_NAME'] = $_SERVER['HTTP_HOST'];
        }

        if (strstr($_SERVER['REQUEST_URI'], '?')) {
            parse_str(substr(strstr($_SERVER['REQUEST_URI'], '?'), 1), $_GET);
        }
    }

    public function __get($name)
    {
        if (isset($this->components[$name])) {
            return $this->components[$name];
        } else {
            $className = '\\Micro\\Core\\' . ucfirst($name) . 'Class';
            if (class_exists($className)) {
                $this->components[$name] = new $className();
                return $this->components[$name];
            } else {
                return false;
            }
        }
    }

    public function __set($name, $value)
    {
        $this->components[$name] = $value;
    }

    public static function httpError($code, $message = '', $exception = null)
    {
        self::$instance->router->response()->code($code);

        if (class_exists('\App\Controller\ErrorPageController')) {
            $errorHandler = new \App\Controller\ErrorPageController;

            self::$instance->router->response()->code($code);

            $result = $errorHandler->handleError($code, $exception);

            if ($result instanceof ResponseInterface) {
                $stack = new EmitterStack();
                $stack->push(new SapiEmitter());

                if ($stack->emit($result) === true) {
                    die();
                }
            }
        }
    }

    // normal error exception processing
    public static function errorException($model, $code = '', $title = '', $trace = '')
    {
        if (\Micro\App::$instance->router->response()->isLocked()) {
            \Micro\App::$instance->router->response()->unlock();
        }

        if ($model instanceof \ErrorException) {
            \Micro\App::$instance->router->response()->code($model->getCode());
            $code = $model->getCode();
        } else {
            \Micro\App::$instance->router->response()->code(500);

            if (!$code) $code = 500;
        }

        $message = '<h2>Error ' . $code . ' .</h2><pre>';
        $message .= '<h3> ' . $title . ' / ' . $code . "</h3><pre style='padding-left:10px'>\n";

        if (\Micro\App::$instance->config['environment'] == 'dev') {
            $message .= "\n" . $trace;
        }

        $message .= "\n</pre>";

        \Micro\App::$instance->router->response()->body($message);

        // clear redirect
        if (\Micro\App::$instance->router->response()->headers()->exists('Location')) {
            \Micro\App::$instance->router->response()->headers()->remove('Location');
        }

        // send body
        \Micro\App::$instance->router->response()->send();

        die();
    }

    public static function errorHandler($title, $message, $data = false, $hiddenData = false)
    {
        echo '<h1>' . $title . '</h1>';
        echo '<p>' . $message . '</p>';
        if ($data) {
            echo '<pre>' . print_r($data, true) . '</pre>';
        }
        die();
    }
}

?>