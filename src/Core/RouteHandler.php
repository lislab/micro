<?php

namespace Micro\Core;

use Invoker\Invoker;
use Invoker\ParameterResolver\Container\TypeHintContainerResolver;
use Klein\Exceptions\HttpException;
use Laminas\HttpHandlerRunner\Emitter\EmitterStack;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Psr\Http\Message\ResponseInterface;

class RouteHandler
{
    private        $app;
    private        $currentHandler = [];
    private static $instance       = false;

    public function __construct()
    {
        self::$instance = $this;
        $this->app = \Micro\App::instance();
    }

    public function globalAction()
    {
        return function ($request, $response, $view, $app) {
            $this->run('global', $request, $response, $app, false);
        };
    }

    public function appAction($handler)
    {
        return function ($request, $response, $view, $app) use ($handler) {
            $this->run('site', $request, $response, $app, $handler);
        };
    }

    public function run($from, $request, $response, $service, $handler)
    {
        parse_str($request->param('query', ''), $query);

        $request->isAjax = ($request->headers()
                                    ->get('x-requested-with') == 'XMLHttpRequest' || $this->app->isJsonInterface === true)
            ? true
            : false;

        // make handler
        if (!$handler) {

            $handler = [
                'module'     => $request->param('module', 'default'),
                'controller' => $request->param('controller', 'index'),
                'action'     => $request->param('action', 'index'),
                'query'      => $query,
            ];
        }

        // check auth require
        if ($this->authRequire()) {
            $handler = [
                'module'     => 'default',
                'controller' => 'auth',
                'action'     => 'login',
            ];
        }

        if (!isset($handler['query'])) {
            $handler['query'] = (empty($query))
                ? []
                : $query;
        }

        // load controller
        $controller = $this->loadModule($handler, $response);

        // controller not found
        if (!$controller) {
            $this->app->router->skipThis();
        }

        // run controller
        if (!is_callable([$controller, ucfirst($handler['action']) . 'Action'])) {
            $this->app->router->skipThis();
        }

        $container = $this->app->getContainer();

        if ($container instanceof InvokerInterface) {
            $invoker = $container;
        } else {
            $containerResolver = new TypeHintContainerResolver($container);

            $invoker = new Invoker(null, $container);
            $invoker->getParameterResolver()->prependResolver($containerResolver);
        }

        try {
            $result = $invoker->call([$controller, ucfirst($handler['action']) . 'Action'], [$handler['query']]);
        } catch (\Exception $e) {
            throw new HttpException(null, 500, $e);
        }

        if ($result instanceof ResponseInterface) {
            $stack = new EmitterStack();
            $stack->push(new SapiEmitter());

            if ($stack->emit($result) === true) {
                die();
            }

            return;
        }

        throw new \UnexpectedValueException('Return value is not instance of ResponseInterface');
    }

    public function activeHandler()
    {
        return $this->currentHandler;
    }

    private function authRequire()
    {
        $currentUser = container()->get(\App\Model\UserModel::class);

        return !$currentUser->auth && $this->app->site->require_auth;
    }

    private function loadModule($handler, $response)
    {

        $module = ($handler['module'] != 'default')
            ? ucfirst($handler['module'])
            : 'default';
        $controller = ucfirst($handler['controller']) . 'Controller';
        $action = ucfirst($handler['action']) . 'Action';

        if ($module == 'default') {
            $controllerClassName = "\\App\\Controller\\" . $controller;
            if (!class_exists($controllerClassName)) {
                $this->app->router->skipThis();
            } else {
                try {
                    $CObject = new $controllerClassName();
                } catch (\Exception $e) {
                    throw new HttpException(null, 500, $e);
                }

                $this->currentHandler = $handler;

                return $CObject;
            }
        }

        if (!$this->checkModuleExists($module, $controller, $response)) {
            $this->app->router->skipThis();
        }

        $controllerClassName = "\\App\\Modules\\" . $module . "\\Controller\\" . $controller;

        if (!class_exists($controllerClassName)) {
            $this->app->router->skipThis();
        }

        try {
            $CObject = new $controllerClassName();
        } catch (\Exception $e) {
            throw new HttpException(null, 500, $e);
        }

        if (!method_exists($CObject, $action)) {
            $this->app->router->skipThis();
        }

        $this->currentHandler = $handler;

        return $CObject;
    }

    private function checkModuleExists($module, $controller, $response)
    {
        $modulepath = $this->app->site->getPath() . 'modules/' . $module . '/';

        if (!file_exists($modulepath)) {
            $this->app->router->skipThis();
        }

        if (!file_exists($modulepath . 'Controller')) {
            $this->app->router->skipThis();
        }

        if (!file_exists($modulepath . 'Controller/' . $controller . '.php')) {
            $this->app->router->skipThis();
        }

        return true;
    }

    public static function instance()
    {
        $className = get_class();
        return (self::$instance)
            ? self::$instance
            : new $className();
    }
}