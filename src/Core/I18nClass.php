<?php 

namespace Micro\Core;

class I18nClass
{
    public $lang;
    
    private $data;
    private $path;
    private $defaultLang;
    
    public function __construct($path, $defaultLang) 
    {
        $this->path = $path;
        $this->defaultLang = $defaultLang;
        $this->loadLang();
    }
    
    public function get($name, $data = false) 
    {
        if(!isset($this->data[$name])) { 
            return '@i18n:'.$name; 
        }
        if(!$data) { 
            return $this->data[$name]; 
        }
        
        $params = array();
        if(is_array($data)) {
            $params = $data;
            array_unshift($this->data[$name]);
        } else {
            $params[] = $this->data[$name];
            $params[] = $data;
        }
        
        return call_user_func_array('sprintf', $params);
    }
    
    public function toObject($name = false) 
    {
        if($name && !isset($this->data[$name])) { 
            return array(); 
        }
        if($name) { 
            return $this->data[$name]; 
        }
        
        return $this->data;
    }
    
    public function plural($name, $count) {
    	$arguments = $this->get($name);
    	$arguments[] = $count;
    	return \Micro\Core\StringHelper::plural(...$arguments);
    }
    
    public function toJson($name = false) 
    {
        return json_encode($this->toObject($name));
    }
    
    private function loadLang() 
    {
        $lang = $this->getlang();
        if(!$lang && !$this->defaultLang) {
            $this->lang = false;
            return false;
        }
                
        $this->lang = ($lang)? $lang : $this->defaultLang;
        $fname = $this->path . $this->lang . '.json';
        
        if(!file_exists($fname)) {
            $this->lang = false;
            return false;
        } 

        $fp = fopen($fname, 'r');
        $source = fread($fp, filesize($fname));
        fclose($fp);

        $this->data = json_decode($source, true);
        
        return $this->data;
    }
    
    private function getLang() 
    {
        if(!empty($_SESSION['lang'])) {
            if(file_exists($this->path . $_SESSION['lang'] . '.json')) { 
                return $_SESSION['lang']; 
            }
        }
        
        if(!empty($_COOKIE['lang'])) {
            if(file_exists($this->path . $_COOKIE['lang'] . '.json')) { 
                return $_COOKIE['lang']; 
            }
        }
        
        return false;
        
    }
}


?>