<?php
namespace Micro\Core;

class UrlHelperClass {

    public function make($query, $data = false, $returnHandler = false) {
        if(is_array($data)) {
            $handler['data'] = $data;
        }
        
        $handler = array();
        if(is_string($query)) {
            if($query == '/') $query = '';
            if(strstr($query, '::')) {
                $query = explode('::', $query);
                if(class_exists($query[0])) {
                    $className = $query[0];

                    if(preg_match("/\/module\/(.?)\//", $className, $match)) {
                        $moduleName = $match[1];
                    } else {
                        $moduleName = 'default';
                    }
                                        
                    $handler = array(
                        'module'=>$moduleName,
                        'controller'=>substr(strrchr($className, '\\'),1),
                        'action'=>$query[1]
                    );
                    
                    return ($returnHandler)? $handler : $this->makeUrlFromHandler($handler);
                } else {
                    return ($returnHandler)? array() : $this->makeUrlFromHandler(array());
                }
            } elseif(strstr($query, '/')) {
                $query = explode("/", $query);
                if(isset($query[0])) {
                    $handler['module'] = $query[0];
                }
                if(isset($query[1])) {
                    $handler['controller'] = $query[1];
                } else {
                    $handler['controller'] = 'index';
                }
                if(isset($query[2])) {
                    $handler['action'] = $query[2];
                } else {
                    $handler['action'] = 'index';
                }
                
                return  ($returnHandler)? $handler : $this->makeUrlFromHandler($handler);
            }
            
            $query = explode(':', $query);
            if(isset($query[0])) {
                $handler['module'] = $query[0];
            }
            if(isset($query[1])) {
                $handler['controller'] = $query[1];
            }
            if(isset($query[2])) {
                $handler['action'] = $query[2];
            }
        } else {
            if(isset($query['module'])) {
                $handler['module'] = $query['module'];
            } elseif(isset($query[0])) {
                $handler['module'] = $query[0];
            }
            
            if(isset($query['controller'])) {
                $handler['controller'] = $query['controller'];
            } elseif(isset($query[1])) {
                $handler['controller'] = $query[1];
            }
            
            if(isset($query['action'])) {
                $handler['action'] = $query['action'];
            } elseif(isset($query[2])) {
                $handler['action'] = $query[2];
            }            
        }
  
        return  ($returnHandler)? $handler : $this->makeUrlFromHandler($handler);
    }
    
    public function get() {
        $handler = \Micro\Core\RouteHandler::instance()->activeHandler();        
        return $this->makeUrlFromHandler($handler);
    }
    
    public function current($query, $depth = false) {
        $current = \Micro\Core\RouteHandler::instance()->activeHandler();
        $query = $this->make($query, false, true);
        
        if($current['module'] == $query['module'] && $current['controller'] == $query['controller']) {
            if($depth && $current['action'] != $query['action']) {
                return false;
            }
            
            return true;
        }
        
        return false;
    }
    
    public function mod() {
        $modules = func_get_args();
        $current = \Micro\Core\RouteHandler::instance()->activeHandler()['module'];
        if(empty($modules)) {
            return $current;
        } else {
            return in_array($current, $modules);
        }
    }
    
    public function ctr() {
        $controllers = func_get_args();
        $current = \Micro\Core\RouteHandler::instance()->activeHandler()['controller'];
        if(empty($controllers)) {
            return $current;
        } else {
            return in_array($current, $controllers);
        }
    }
    
    public function act() {
        $actions = func_get_args();
        $current = \Micro\Core\RouteHandler::instance()->activeHandler()['action'];
        if(empty($actions)) {
            return $current;
        } else {
            return in_array($current, $actions);
        }
    }
    
    private function makeUrlFromHandler($handler) {
        $url = \Micro\App::instance()->site->prefix;
        
        if(!empty($handler['module'])) {
            $url .= $handler['module'];
        } else {
            return $url;
        }
    
        if(!empty($handler['controller'])) {
            $url .= '/' . $handler['controller'];
        } else {
            return $url;
        }
    
        if(!empty($handler['action'])) {
            $url .= '/' . $handler['action'];
        } else {
            return $url;
        }
         
        if(!empty($handler['data'])) {
            $url .= '/?' .  http_build_query($handler['data']);
        } else {
            return $url;
        }
        
        return $url;
    }
    
}