<?php
namespace Micro\Core;

class SiteConfigClass extends ModelClass
{
    
    public $id = false;
    public $domain = false;
    public $static_path = '/';
    public $theme = '';
    public $require_auth = false;
    public $prefix = ''; 
    public $match_position = 0;
    public $no_auth_user_id = 0;
    public $layout_path = 'layout';
    
    public $config = '';
    public $path = '/';
    
    public function init() 
    {
        $prefix = $_SERVER['REQUEST_URI'];
        $domain = $_SERVER['SERVER_NAME'];

        $config = container()->get('config');
     
        if(strlen($prefix) > 1) {
            $prefix = '/' . strstr(substr($prefix, 1), '/', true). '/';
        }
        
        if(empty($config['app']) || !is_array($config['app'])) {
            \Micro\App::errorHandler('Site error', 'Error load site config!', 'Config store: local file. Site not configured. Use "app" key in config.php for configure site.');
        }
        
        $site = $this->findSite($domain, $prefix);
        $this->makeModel($site['info']);
        
        if(empty($this->id)) {
            \Micro\App::errorHandler('Site error', 'Error load site model!');
        }
        
        $this->loadSiteConfig();

        return true;
    }
        
    public function getStaticPath() {
        return $this->config['static'];
    }
    
    public function getPath() 
    {
        return ROOTPATH .  $this->path;
    }
    
    public function getLayoutPath()
    {
        return $this->getPath() . $this->layout_path;
    }

    private function loadSiteConfig() {
        $configFile = ROOTPATH . $this->path . 'config.php';
        
        if(!file_exists($configFile)) {
            \Micro\App::errorHandler('Site error', 'Config not found! Path: '.$this->path . 'config.php', array('file'=>$configFile));
        }
        try {
            $this->config = include $configFile;
        } catch (\Exception $e) {
            \Micro\App::errorHandler('Site error', 'Error load site config!', array('message'=>$e->getMessage()));
            return false;
        }
        
        if(!empty($this->config['static'])) {
            $this->static_path = $this->config['static'];
        }        
        
        if(!empty($this->config['theme'])) {
            $this->theme = $this->config['theme'];
            $this->layout_path = $this->layout_path . '/' . $this->theme;
        }
    }
      
    private function getLocations() {
        $locations = array();

        $config = container()->get('config');
        
        foreach($config['app'] as $index=>$site) {
            $domains = $site['domain'];
            
            if(!isset($site['id'])) {
                $site['id'] = $index+1;
            }

            if(is_array($domains)) {
                unset($site['domain']);
                foreach($domains as $k=>$item) {
                    $locations[] = array(
                        'location' => $item . ((isset($site['prefix']))? $site['prefix'] : ''),
                        'info' => $site,
                    );
                }
            } else {
                $locations[] = array(
                    'location'=>$domains . ((isset($site['prefix']))? $site['prefix'] : ''),
                    'info'=>$site
                );
            }
        } 
        
        return $locations;
    }
    
    private function findSite($domain, $prefix) {
        $locations = $this->getLocations();

        $finded = array_search($domain . $prefix, array_column($locations, 'location'));
        if($finded !== false) {
            return $locations[$finded];
        }
        
        $finded = array_search($domain . '/', array_column($locations, 'location'));
        
        if($finded !== false) {
            return $locations[$finded];
        }
        
        \Micro\App::errorHandler('Site error', 'This location not configured', array('domain'=>$domain, 'prefix'=>$prefix, 'locations'=>$locations));
        
        return false;
    }
}
