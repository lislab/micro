<?php
namespace Micro\Pdo;


class AppPDOStatement extends \PDOStatement
{
	public $fullQuery;

	protected $_pdo = "";
	protected $boundParams = array();


	protected function __construct($pdo)
	{
		if ($pdo instanceof \PDO) {
			$this->_pdo = $pdo;
		}
	}

	public function bindParam($param, &$value, $datatype = \PDO::PARAM_STR, $length = 0, $driverOptions = false)
	{
		$this->boundParams[$param] = &$value;
		return parent::bindParam($param, $value, $datatype, $length, $driverOptions);
	}

	public function bindValue($param, $value, $datatype = \PDO::PARAM_STR)
	{
		$this->boundParams[$param] = $value;
		return parent::bindValue($param, $value, $datatype);
	}

	public function interpolateQuery($inputParams = null)
	{
		$testQuery = $this->queryString;
		/**
		 * If parameters were bound prior to execution, boundParams will be true
		 */
		if ($this->boundParams) {
			// We ksort our bound parameters array to allow parameter binding to numbered ? markers and we need to
			// replace them in the correct order
			ksort($this->boundParams);

			foreach ($this->boundParams as $key => $array) {
				/**
				 * UPDATE - Issue #3
				 * It is acceptable for bound parameters to be provided without the leading :, so if we are not matching
				 * a ?, we want to check for the presence of the leading : and add it if it is not there.
				 */
				if (is_numeric($key)) {
					$key    = "\?";
				} else {
					$key    = (preg_match("/^\:/", $key)) ? $key : ":" . $key;
				}
				$value      = $array;
				$testParam  = "/" . $key . "(?!\w)/";
				$replValue  = $this->_prepareValue($value);
				$testQuery  = preg_replace($testParam, $replValue, $testQuery, 1);
			}
		}
		/**
		 * Otherwise, if we have input parameters, we'll replace ? markers
		 * UPDATE - we can now accept $key => $value named parameters as well:
		 * $inputParams = array(
		 *   ":username" => $username
		 * , ":password" => $password
		 * );
		 */
		if (is_array($inputParams) && $inputParams !== array()) {
			ksort($inputParams);
			$testQuery = $this->sql_debug($testQuery, $inputParams);
		}
		$this->fullQuery = $testQuery;
		return $testQuery;
	}

	private function sql_debug($sql_string, array $params = null) {
		if (!empty($params)) {
			$indexed = $params == array_values($params);
			foreach($params as $k=>$v) {
				if (is_object($v)) {
					if ($v instanceof \DateTime) $v = $v->format('Y-m-d H:i:s');
					else continue;
				}
				elseif (is_string($v)) $v="'$v'";
				elseif ($v === null) $v='NULL';
				elseif (is_array($v)) $v = implode(',', $v);

				if ($indexed) {
					$sql_string = preg_replace('/\?/', $v, $sql_string, 1);
				}
				else {
					if ($k[0] != ':') $k = ':'.$k; //add leading colon if it was left out
					$sql_string = str_replace($k,$v,$sql_string);
				}
			}
		}
		return $sql_string;
	}

	public function execute($inputParams = null)
	{
		$this->interpolateQuery($inputParams);
		return parent::execute($inputParams);
	}

	private function _prepareValue($value)
	{
		if ($this->_pdo && ($this->_pdo instanceof \PDO)) {
			$value = $this->_pdo->quote($value);
		} else {
			$value = "'" . addslashes($value) . "'";
		}
		return $value;
	}

}

?>