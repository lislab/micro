<?php 

namespace Micro\Pdo;

use PDOStatement;

class AppDatabase
{
    private $connections = array();
    private $connectionName = 'default';
    private $cache = array('query'=>array(), 'meta'=>array());


    /**
     * @var false|PDOStatement|AppPDOStatement
     */
    private $lastQuery = false;
    
    public $error = '';
    public $foundRows = false;

    public function connect(Array $config, $name = 'default', $environment = 'production')
    {
        if(isset($this->connections[$name])) {
            $this->connectionName = $name;
            return $this->connections[$name];
        } else {
            try {
                $pdo = new \PDO(
                    "mysql:host={$config['host']};dbname={$config['dbname']}",
                    $config['user'],
                    $config['pass']
                );      
                if($environment == 'dev') {
                    $pdo->setAttribute(\PDO::ATTR_STATEMENT_CLASS, array("Micro\Pdo\AppPDOStatement", array($pdo)));
                }
            } catch (\PDOException $e) {
                $this->error = $e->getMessage();
                
                return false;
            }
        }
        
        $this->connectionName = $name;
        $this->connections[$name] = $pdo;
        
        return true;
    }
    
    public function query($query, $params = null) 
    {
        $this->error = null;
        $this->prepare($query);
        $this->execute($params);
        
        return $this;
    }
    
    public function prepare($query) 
    {        
        $this->lastQuery = $this->connection()->prepare($query);
        return $this->lastQuery;
    }
    
    public function getTableMeta($table) {
        if(isset($this->cache['meta'][$table])) {
            return $this->cache['meta'][$table];
        }
        $query = $this->connection()->prepare("DESCRIBE {$table}");
        $query->execute();
        $table_fields = $query->fetchAll(\PDO::FETCH_COLUMN);

        $this->cache['meta'][$table] = $table_fields;
        
        return $table_fields;
    }
    
    public function exec($params = null) 
    {
        return $this->execute($params);
    }
    
    public function execute($params = null) 
    {
        if(!$this->lastQuery) { 
            return false; 
        }
        $result = $this->lastQuery->execute($params);
        if(!$result) {
            $this->error = $this->lastQuery->errorInfo();
            if(is_array($this->error)) {
                $this->error = 'SQL error #'.$this->error[1].' - '.$this->error[2];
            }
        }
        
        return $this;
    }
    
    public function getOne() 
    {
        if(!$this->lastQuery) { 
            return false; 
        }
        return $this->lastQuery->fetch(\PDO::FETCH_ASSOC);
    }
    
    public function getAll($calcFoundedRows = false, $plain = false) 
    {
        if(!$this->lastQuery) { 
            return false; 
        }
        
        if($plain) {
        	$data = $this->lastQuery->fetchAll(\PDO::FETCH_COLUMN);
        } else {
        	$data = $this->lastQuery->fetchAll(\PDO::FETCH_ASSOC);
        }
        
        
        
        if($calcFoundedRows) {
            $this->query("SELECT FOUND_ROWS()");
            $this->foundRows = $this->getField();
        } else {
            $this->foundRows = false;
        }
        
        
        return $data;
    }
    
    public function each($callback) {
        if(is_callable($callback)) {
            $data = array();
            while($row = $this->lastQuery->fetch(\PDO::FETCH_ASSOC)) {
                $result = call_user_func($callback, array(&$row));
                if(empty($result)) {
                    array_merge($data, $row);
                } else {
                    $data[] = $result;
                }
            }
            
            return $data;
        }
    }
    
    public function fetchAll() {
        return $this->getAll();
    }
    
    public function getField($num = 0) 
    {
        if(!$this->lastQuery) { 
            return false; 
        }
        return $this->lastQuery->fetchColumn($num);
    }
    
    public function getSql($format = true) 
    {
        if(!$this->lastQuery) { 
            return false; 
        }
        
        return $this->lastQuery->fullQuery ?? $this->lastQuery->queryString;
    }
    
    public function saveAll($data, $table, $primary = false) 
    {
        if(empty($data) || !is_array($data)) {
            $this->error = 'Data error: Empty save database request';
            return false;
        }
        
        if(empty($table) || !is_string($table)) {
            $this->error = 'Data error: Table not specified on save database request';
            return false;
        }
        
        $result = array();
        
        foreach($data as $k=>$item) {
            $result[$k] = $this->save($item, $table, $primary);
        }
        
        return $result;
    }
    
    public function numRows() {
        if(!$this->lastQuery) {
            return false;
        }
        return $this->lastQuery->rowCount();
    }
    
    public function rowCount() {
        return $this->numRows();
    }
    
    public function save($data, $table, $primary = false) 
    {
        if(empty($data) || !is_array($data)) {
            $this->error = 'Data error: Empty save database request';
            return false;
        }
        
        if(empty($table) || !is_string($table)) {
            $this->error = 'Data error: Table not specified on save database request';
            return false;
        }
        
        $cmd = '';
        $where = '';
        $wheredata = array();
        if(empty($primary)) {
            $cmd = 'INSERT';
        } else {
            if(is_string($primary)) {
                if(!isset($data[$primary])) {
                    $cmd = 'INSERT';
                } else {
                    $where = "`{$primary}` = :pval1";
                    $wheredata = array('pval1'=>$data[$primary]);
                    
                    $this->query("SELECT 1 FROM `{$table}` WHERE {$where} LIMIT 1", $wheredata);
                    if(intval($this->getField())) {
                        $cmd = 'UPDATE';
                    } else {
                        $cmd = 'INSERT';
                        $where = '';
                        $wheredata = array();
                    }
                }
            } elseif(is_array($primary)) {
                $whereArray = array();
                $index = 0;
                foreach($primary as $item) {
                    if(!isset($data[$item])) {
                        $cmd = 'INSERT';
                        break;
                    } else {
                        $whereArray[] = '`' . $item . '` = :pval' . $index;
                        $wheredata['pval' . $index] = $data[$item];
                    }
                    $index++;
                }
                if(!$cmd) {
                    $where = implode(' AND ', $whereArray);
                    $this->query("SELECT 1 FROM `{$table}` WHERE {$where} LIMIT 1", $wheredata);                    
                    if(intval($this->getField())) {
                        $cmd = 'UPDATE';
                    } else {
                        $cmd = 'INSERT';
                        $where = '';
                        $wheredata = array();
                    }
                } else {
                    $where = '';
                    $wheredata = array();
                }
            }
        }
        
        if($cmd == 'INSERT') {
            $fields = array();
            $keys = array();
            $queryData = array();
            $index = 0;
            foreach($data as $field => $value) {
                $fields[] = $field;
                $keys[] = ':ivalue' . $index;
                $queryData[':ivalue'.$index] = $value;
                $index++;
            }
            $fields = '`' . implode('`,`', $fields) . '`';
            $keys = implode(',', $keys);
            
            $this->lastQuery = $this->connection()->prepare("INSERT INTO `{$table}` ({$fields}) VALUE({$keys})");
            $result = $this->lastQuery->execute($queryData);

            if($result === false) {
                $this->error = $this->lastQuery->errorInfo();
                if(is_array($this->error)) {
                    $this->error = 'SQL error #'.$this->error[1].' - '.$this->error[2];
                }
                return false;
            } else {
                return $this->connection()->lastInsertId();
            }
        } else {
            $fields = array();
            $index = 0;
            $queryData = array();
            
            foreach($data as $field => $value) {
                $fields[] = "`{$field}` = :ivalue{$index}";
                $queryData[':ivalue'.$index] = $value;
                $index++;
            }
            
            $fields = implode(',', $fields);
            $this->lastQuery = $this->connection()->prepare("UPDATE `{$table}` SET {$fields} WHERE {$where}");
            $queryData = array_merge($queryData, $wheredata);
            $result = $this->lastQuery->execute($queryData);
            
            if($result === false) {
                $this->error = $this->lastQuery->errorInfo();
                if(is_array($this->error)) {
                    $this->error = 'SQL error #'.$this->error[1].' - '.$this->error[2];
                }
            }
            
            
            return ($result === false)? false : true;
        }
    }
    
    
    public function debug() 
    {
        print_r($this->getSql());
    }
    
    private function connection() 
    {
        return $this->connections[$this->connectionName];
    }
} 

?>